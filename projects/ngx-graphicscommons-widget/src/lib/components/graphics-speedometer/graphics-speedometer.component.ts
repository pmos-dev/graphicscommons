import { Component, Input } from '@angular/core';

import { CommonsThemeComponent } from 'ngx-webappcommons-core';

import { CommonsThemeService } from 'ngx-webappcommons-core';

const SIZE: number = 400;

@Component({
	selector: 'graphics-speedometer',
	templateUrl: './graphics-speedometer.component.html',
	styleUrls: ['./graphics-speedometer.component.less']
})
export class GraphicsSpeedometerComponent extends CommonsThemeComponent {
	SIZE: number = SIZE;

	@Input() value: number = 0;
	@Input() secondary: number|undefined;
	@Input() average: boolean = false;
	@Input() scale: number[] = [];

	constructor(
		themeService: CommonsThemeService
	) {
		super(themeService);
	}

	private getRotation(p: number): number {
		if (p < this.scale[0]) p = this.scale[0];
		if (p > this.scale[this.scale.length - 1]) p = this.scale[this.scale.length - 1] - 0.1;

		const zero = -135;
		const interval = 30;
		
		let i = -1;
		let last = 0;
		for (const scale of this.scale) {
			i++;
			if (p >= scale) {
				last = scale;
				continue;
			}
			const delta = p - scale;
			const divisor = scale - last;
			const ratio = delta / divisor;
			
			return zero + (interval * i) + (interval * ratio);
		}
		
		return 0;
	}

	getNeedleRotation(): number {
		return this.getRotation(this.value);
	}

	getSecondaryNeedleRotation(): number {
		if (this.secondary === undefined) return 0;
		return this.getRotation(this.secondary);
	}
}
