import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsPipeModule } from 'ngx-angularcommons-pipe';

import { NgxGraphicsCommonsCoreModule } from 'ngx-graphicscommons-core';

import { GraphicsSpeedometerComponent } from './components/graphics-speedometer/graphics-speedometer.component';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsPipeModule,
				NgxGraphicsCommonsCoreModule
		],
		declarations: [
				GraphicsSpeedometerComponent
		],
		exports: [
				GraphicsSpeedometerComponent
		]
})
export class NgxGraphicsCommonsWidgetModule { }
