/*
 * Public API Surface of ngx-graphicscommons-widget
 */

export * from './lib/components/graphics-speedometer/graphics-speedometer.component';

export * from './lib/ngx-graphicscommons-widget.module';
