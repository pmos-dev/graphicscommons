import { Component, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: 'graphics-svg',
		templateUrl: './graphics-svg.component.html',
		styleUrls: ['./graphics-svg.component.less']
})
export class GraphicsSvgComponent extends CommonsComponent {
	@Input() width: number = 100;
	@Input() height: number = 100;

	constructor() {
		super();
	}

	getViewBox(): string {
		return `0 0 ${this.width} ${this.height}`;
	}
}
