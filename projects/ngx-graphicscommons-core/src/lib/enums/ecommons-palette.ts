import { CommonsType } from 'tscommons-core';

export enum ECommonsPalette {
		PRIMARY = 'primary',
		PRIMARY_LIGHT = 'primary-light',
		PRIMARY_DARK = 'primary-dark',
		SECONDARY = 'secondary',
		SECONDARY_LIGHT = 'secondary-light',
		SECONDARY_DARK = 'secondary-dark'
}

export function toECommonsPalette(type: string): ECommonsPalette|undefined {
	switch (type) {
		case ECommonsPalette.PRIMARY.toString():
			return ECommonsPalette.PRIMARY;
		case ECommonsPalette.PRIMARY_LIGHT.toString():
			return ECommonsPalette.PRIMARY_LIGHT;
		case ECommonsPalette.PRIMARY_DARK.toString():
			return ECommonsPalette.PRIMARY_DARK;
		case ECommonsPalette.SECONDARY.toString():
			return ECommonsPalette.SECONDARY;
		case ECommonsPalette.SECONDARY_LIGHT.toString():
			return ECommonsPalette.SECONDARY_LIGHT;
		case ECommonsPalette.SECONDARY_DARK.toString():
			return ECommonsPalette.SECONDARY_DARK;
	}
	return undefined;
}

export function fromECommonsPalette(type: ECommonsPalette): string {
	switch (type) {
		case ECommonsPalette.PRIMARY:
			return ECommonsPalette.PRIMARY.toString();
		case ECommonsPalette.PRIMARY_LIGHT:
			return ECommonsPalette.PRIMARY_LIGHT.toString();
		case ECommonsPalette.PRIMARY_DARK:
			return ECommonsPalette.PRIMARY_DARK.toString();
		case ECommonsPalette.SECONDARY:
			return ECommonsPalette.SECONDARY.toString();
		case ECommonsPalette.SECONDARY_LIGHT:
			return ECommonsPalette.SECONDARY_LIGHT.toString();
		case ECommonsPalette.SECONDARY_DARK:
			return ECommonsPalette.SECONDARY_DARK.toString();
	}
	
	throw new Error('Unknown ECommonsPalette');
}

export function isECommonsPalette(test: unknown): test is ECommonsPalette {
	if (!CommonsType.isString(test)) return false;
	
	return toECommonsPalette(test) !== undefined;
}

export function keyToECommonsPalette(key: string): ECommonsPalette {
	switch (key) {
		case 'PRIMARY':
			return ECommonsPalette.PRIMARY;
		case 'PRIMARY_LIGHT':
			return ECommonsPalette.PRIMARY_LIGHT;
		case 'PRIMARY_DARK':
			return ECommonsPalette.PRIMARY_DARK;
		case 'SECONDARY':
			return ECommonsPalette.SECONDARY;
		case 'SECONDARY_LIGHT':
			return ECommonsPalette.SECONDARY_LIGHT;
		case 'SECONDARY_DARK':
			return ECommonsPalette.SECONDARY_DARK;
	}
	
	throw new Error(`Unable to obtain ECommonsPalette for key: ${key}`);
}

export const ECOMMONS_PALETTES: ECommonsPalette[] = Object.keys(ECommonsPalette)
		.map((e: string): ECommonsPalette => keyToECommonsPalette(e));
