import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GraphicsSvgComponent } from './components/graphics-svg/graphics-svg.component';

@NgModule({
		imports: [
				CommonModule
		],
		declarations: [
				GraphicsSvgComponent
		],
		exports: [
				GraphicsSvgComponent
		]
})
export class NgxGraphicsCommonsCoreModule { }
