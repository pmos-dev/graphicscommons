import { NgZone, Input, OnInit, OnDestroy, Directive } from '@angular/core';
import { ViewChild, ElementRef, HostListener } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

export abstract class GraphicsLongLatMapComponent extends CommonsComponent implements OnInit, OnDestroy {
	@ViewChild('canvas', { static: true }) canvas!: ElementRef;
	@ViewChild('section', { static: true }) section!: ElementRef;

	@Input() aspectRatio: number = 0;
	@Input() background: string = '';
	
	width: number = 0;
	height: number = 0;

	private ctx: CanvasRenderingContext2D|undefined;
	private backgroundMap: HTMLImageElement|undefined;
	private scaledBackground: HTMLCanvasElement|undefined;

	private isBackgroundLoaded: boolean = false;
	private isScaledBackgroundLoaded: boolean = false;
	private isComponentActive: boolean = false;

	@HostListener('window:resize', ['$event']) onResize(_event: any) {
		this.resize();
	}
	
	constructor(
			private redrawPeriod: number = 250,
			private zone: NgZone
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		this.isComponentActive = true;

		this.ctx = this.canvas.nativeElement.getContext('2d');
		this.backgroundMap = new Image();
		this.backgroundMap.onload = () => {
			this.isBackgroundLoaded = true;
			
			this.resize();
			
			this.internalPaint();
		};
		
		this.backgroundMap.src = this.background;
		
		this.resize();
	}

	ngOnDestroy(): void {
		this.isComponentActive = false;

		super.ngOnDestroy();
	}
	
	private resize() {
		this.width = this.section.nativeElement.scrollWidth;
		this.height = this.width * this.aspectRatio;
		
		this.zone.runOutsideAngular((): void => {
			if (this.isBackgroundLoaded) {
				const resized: HTMLCanvasElement = document.createElement('canvas');
				resized.width = this.width;
				resized.height = this.height;
				
				const ctx: CanvasRenderingContext2D|null = resized.getContext('2d');
				if (ctx === null) return;
				
				ctx.drawImage(this.backgroundMap!, 0, 0, this.width, this.height);
				
				this.scaledBackground = resized;
				
				this.isScaledBackgroundLoaded = true;
			}
		});
	}

	protected abstract paint(ctx: CanvasRenderingContext2D): void;
	
	private internalPaint() {
		this.zone.runOutsideAngular((): void => {
			setTimeout(() => requestAnimationFrame(() => this.internalPaint()), this.redrawPeriod);
	
			if (!this.backgroundMap || !this.scaledBackground || !this.ctx) return;
			if (!this.isComponentActive || !this.isBackgroundLoaded || !this.isScaledBackgroundLoaded) return;
	
			this.ctx.clearRect(0, 0, this.width, this.height);
			this.ctx.drawImage(this.scaledBackground, 0, 0, this.width, this.height);
			
			this.paint(this.ctx);
		});
	}
}
