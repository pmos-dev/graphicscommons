import { Component, NgZone, Input } from '@angular/core';

import { CommonsColor } from 'tscommons-graphics';
import { IRgb } from 'tscommons-graphics';
import { EHsla } from 'tscommons-graphics';
import { ERgba } from 'tscommons-graphics';

import { GraphicsLongLatMapComponent } from '../graphics-long-lat-map/graphics-long-lat-map.component';

import { ECommonsMapRegion } from '../../enums/ecommons-map-region';
import { TCommonsPoint } from '../../types/tcommons-point';

@Component({
	selector: 'graphics-heat-map',
	templateUrl: './graphics-heat-map.component.html',
	styleUrls: ['./graphics-heat-map.component.less']
})
export class GraphicsHeatMapComponent extends GraphicsLongLatMapComponent {
	@Input() region: ECommonsMapRegion|undefined;
	@Input() data: TCommonsPoint[] = [];
	@Input() power: number = 0.5;
	@Input() percentiles: boolean = false;

	constructor(
			zone: NgZone
	) {
		super(2000, zone);
	}

	protected paint(ctx: CanvasRenderingContext2D): void {
		const snapshot: TCommonsPoint[] = this.data.slice();
		let max: number = 0;
		const percentiles: number[] = [];
		
		/*if (this.percentiles) {
			let tallies: number[] = [], clone: TCommonsPoint[] = [];
			for (let point of this.data) {
				tallies.push(point.t);
				clone.push({
					xy: {
						x: point.xy.x,
						y: point.xy.y
					},
					t: point.t,
				});
			}
			
			tallies.sort((a: number, b: number): number => {
				if (a < b) return -1;
				if (a > b) return 1;
				return 0;
			});
	
			for (let i = 0; i <= 1; i += 0.01) percentiles.push(tallies[Math.floor(tallies.length * i)]);
	
			clone.sort((a: TCommonsPoint, b: TCommonsPoint) => {
				if (a.t < b.t) return -1;
				if (a.t > b.t) return 1;
				return 0;
			});
			
			snapshot = clone;
		} else {*/
		max = 1;
		for (const point of this.data) max = Math.max(max, point.t);
		/*}*/
		
		for (const point of snapshot) {
			let x: number = point.xy.x;
			let y: number = point.xy.y;
			
			switch (this.region) {
				case ECommonsMapRegion.WORLD:
					x *= 0.955;
					x += 0.02;
					
					y *= 1.31;
					y -= 0.07;

					break;
				case ECommonsMapRegion.UK:
					x -= 0.5;
					x *= 29.4;
					x += 0.854;
					
					y -= 0.5;
					y *= 21.8;
					y += 4;
					y += 0.5;
					
					break;
			}

			x *= this.canvas.nativeElement.width;
			y *= this.canvas.nativeElement.height;
			
			let color: IRgb;
			let opacity;
			
			if (this.percentiles) {
				let index = -1;
				for (const p of percentiles) {
					index++;
					if (point.t < p) {
						break;
					}
				}
	
				color = CommonsColor.hslToRgb({
						[EHsla.HUE]: ((percentiles.length - 1) - index) / percentiles.length,
						[EHsla.SATURATION]: 1,
						[EHsla.LIGHTNESS]: 0.5
				});
				opacity = index / percentiles.length;
			} else {
				const value = Math.pow(point.t / max, this.power);
				color = CommonsColor.hslToRgb({
						[EHsla.HUE]: (1 - value) * 0.7,
						[EHsla.SATURATION]: 1,
						[EHsla.LIGHTNESS]: 0.5
				});
				opacity = value;
			}
			
			ctx.beginPath();
			ctx.arc(x, y, this.canvas.nativeElement.width * 0.014, 0, 2 * Math.PI, false);
			ctx.fillStyle = `rgba(${color[ERgba.RED]},${color[ERgba.GREEN]},${color[ERgba.BLUE]},${opacity * 0.1})`;
			ctx.fill();

			ctx.beginPath();
			ctx.arc(x, y, this.canvas.nativeElement.width * 0.01, 0, 2 * Math.PI, false);
			ctx.fillStyle = `rgba(${color[ERgba.RED]},${color[ERgba.GREEN]},${color[ERgba.BLUE]},${opacity * 0.2})`;
			ctx.fill();

			ctx.beginPath();
			ctx.arc(x, y, this.canvas.nativeElement.width * 0.006, 0, 2 * Math.PI, false);
			ctx.fillStyle = `rgba(${color[ERgba.RED]},${color[ERgba.GREEN]},${color[ERgba.BLUE]},${opacity * 0.3})`;
			ctx.fill();

			ctx.beginPath();
			ctx.arc(x, y, this.canvas.nativeElement.width * 0.002, 0, 2 * Math.PI, false);
			ctx.fillStyle = `rgba(${color[ERgba.RED]},${color[ERgba.GREEN]},${color[ERgba.BLUE]},${opacity * 0.4})`;
			ctx.fill();
		}
	}
}
