import { Component, NgZone, Input } from '@angular/core';

import { CommonsColor } from 'tscommons-graphics';
import { IRgb } from 'tscommons-graphics';
import { EHsla } from 'tscommons-graphics';
import { ERgba } from 'tscommons-graphics';

import { GraphicsLongLatMapComponent } from '../graphics-long-lat-map/graphics-long-lat-map.component';

import { ECommonsMapRegion } from '../../enums/ecommons-map-region';
import { TCommonsPoint } from '../../types/tcommons-point';

@Component({
	selector: 'graphics-live-map',
	templateUrl: './graphics-live-map.component.html',
	styleUrls: ['./graphics-live-map.component.less']
})
export class GraphicsLiveMapComponent extends GraphicsLongLatMapComponent {
	@Input() region: ECommonsMapRegion|undefined;
	@Input() data: TCommonsPoint[] = [];

	constructor(
			zone: NgZone
	) {
		super(250, zone);
	}

	protected paint(ctx: CanvasRenderingContext2D): void {
		const snapshot: TCommonsPoint[] = this.data.slice();
		
		for (const point of snapshot) {
			let x: number = point.xy.x;
			let y: number = point.xy.y;
			
			switch (this.region) {
				case ECommonsMapRegion.WORLD:
					x *= 0.955;
					x += 0.02;
					
					y *= 1.31;
					y -= 0.07;

					break;
				case ECommonsMapRegion.UK:
					x -= 0.5;
					x *= 29.4;
					x += 0.854;
					
					y -= 0.5;
					y *= 21.8;
					y += 4;
					y += 0.5;
					
					break;
			}

			x *= this.canvas.nativeElement.width;
			y *= this.canvas.nativeElement.height;
			
			const color: IRgb = CommonsColor.hslToRgb({
					[EHsla.HUE]: 0,
					[EHsla.SATURATION]: 1,
					[EHsla.LIGHTNESS]: 0.5
			});
			
			let width: number = 0;
			let opacity: number = 0;
			switch (point.t) {
//				case 8: width = 0.026; opacity = 0.05; break;
//				case 7: width = 0.023; opacity = 0.1; break;
//				case 6: width = 0.020; opacity = 0.2; break;
//				case 5: width = 0.017; opacity = 0.3; break;
//				case 4: width = 0.014; opacity = 0.5; break;
//				case 3: width = 0.011; opacity = 0.6; break;
//				case 2: width = 0.008; opacity = 0.8; break;
//				case 1: width = 0.005; opacity = 1; break;
				//case 8: width = 0.026; opacity = 0.05; break;
				case 4: width = 0.029; opacity = 0.1; break;
				//case 6: width = 0.020; opacity = 0.2; break;
				case 3: width = 0.021; opacity = 0.3; break;
				//case 4: width = 0.014; opacity = 0.5; break;
				case 2: width = 0.013; opacity = 0.6; break;
				//case 2: width = 0.008; opacity = 0.8; break;
				case 1: width = 0.005; opacity = 1; break;
			}
			
			ctx.beginPath();
			ctx.arc(x, y, this.canvas.nativeElement.width * width, 0, 2 * Math.PI, false);
			ctx.fillStyle = `rgba(${color[ERgba.RED]},${color[ERgba.GREEN]},${color[ERgba.BLUE]},${opacity})`;
			ctx.fill();
		}
	}
}
