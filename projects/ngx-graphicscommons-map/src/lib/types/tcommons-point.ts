import { TCommonsXy } from 'ngx-graphicscommons-core';

export type TCommonsPoint = {
		xy: TCommonsXy;
		t: number;
};
