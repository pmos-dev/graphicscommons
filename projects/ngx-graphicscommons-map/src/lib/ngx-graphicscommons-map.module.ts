import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxGraphicsCommonsCoreModule } from 'ngx-graphicscommons-core';

import { GraphicsHeatMapComponent } from './components/graphics-heat-map/graphics-heat-map.component';
import { GraphicsLiveMapComponent } from './components/graphics-live-map/graphics-live-map.component';

@NgModule({
		imports: [
				CommonModule,
				NgxGraphicsCommonsCoreModule
		],
		declarations: [
				GraphicsHeatMapComponent,
				GraphicsLiveMapComponent
		],
		exports: [
				GraphicsHeatMapComponent,
				GraphicsLiveMapComponent
		]
})
export class NgxGraphicsCommonsMapModule { }
