import { CommonsType } from 'tscommons-core';

export enum ECommonsMapRegion {
		UK = 'uk',
		WORLD = 'world'
}

export function toECommonsMapRegion(type: string): ECommonsMapRegion|undefined {
	switch (type) {
		case ECommonsMapRegion.UK.toString():
			return ECommonsMapRegion.UK;
		case ECommonsMapRegion.WORLD.toString():
			return ECommonsMapRegion.WORLD;
	}
	return undefined;
}

export function fromECommonsMapRegion(type: ECommonsMapRegion): string {
	switch (type) {
		case ECommonsMapRegion.UK:
			return ECommonsMapRegion.UK.toString();
		case ECommonsMapRegion.WORLD:
			return ECommonsMapRegion.WORLD.toString();
	}
	
	throw new Error('Unknown ECommonsMapRegion');
}

export function isECommonsMapRegion(test: unknown): test is ECommonsMapRegion {
	if (!CommonsType.isString(test)) return false;
	
	return toECommonsMapRegion(test) !== undefined;
}

export function keyToECommonsMapRegion(key: string): ECommonsMapRegion {
	switch (key) {
		case 'UK':
			return ECommonsMapRegion.UK;
		case 'WORLD':
			return ECommonsMapRegion.WORLD;
	}
	
	throw new Error(`Unable to obtain ECommonsMapRegion for key: ${key}`);
}

export const ECOMMONS_MAP_REGIONS: ECommonsMapRegion[] = Object.keys(ECommonsMapRegion)
		.map((e: string): ECommonsMapRegion => keyToECommonsMapRegion(e));
