export interface ICommonsSegment {
		value: number;
		label?: string;
		color?: string;
}
