export interface ICommonsLine {
		value: number;
		name: string;
		major?: boolean;
}
