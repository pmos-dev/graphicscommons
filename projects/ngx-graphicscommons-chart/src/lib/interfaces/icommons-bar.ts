export interface ICommonsBar {
		value: number;
		name: string;
		label?: string;
		color?: string;
		major?: boolean;
}
