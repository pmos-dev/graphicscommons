export interface ICommonsRange {
		from: number;
		to: number;
		name: string;
		label?: string;
		color?: string;
		major?: boolean;
}
