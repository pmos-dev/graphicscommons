export interface ICommonsMultiLine {
		values: number[];
		name: string;
		major?: boolean;
}
