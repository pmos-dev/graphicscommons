import { SafeStyle } from '@angular/platform-browser';

export interface ICommonsColor {
		classColor?: string;
		styleColor?: SafeStyle;
}
