import { DomSanitizer } from '@angular/platform-browser';

import { ECommonsColor, fromECommonsColor } from 'tscommons-graphics';

import { CommonsPrettyFigurePipe } from 'ngx-angularcommons-pipe';

import { TCommonsXy } from 'ngx-graphicscommons-core';
import { ECommonsPalette, fromECommonsPalette } from 'ngx-graphicscommons-core';

import { ICommonsColor } from '../interfaces/icommons-color';

export abstract class GraphicsCharts {
	public static listRainbowDistribution(length: number): ECommonsColor[] {
		let rainbowIndex: number = 0;
		const rainbowInc: number = length === 0 ? 1 : Math.max(1, (Object.keys(ECommonsColor).length / length));
		
		const distribution: ECommonsColor[] = [];
		for (let i: number = 0; i < length; i++) {
			let index: number = Math.round(rainbowIndex);
			if (index < 0) index = 0;
			if (index >= Object.keys(ECommonsColor).length) index = Object.keys(ECommonsColor).length - 1;
			
			switch (index) {
				case 0:
					distribution.push(ECommonsColor.RED);
					break;
				case 1:
					distribution.push(ECommonsColor.ORANGE);
					break;
				case 2:
					distribution.push(ECommonsColor.YELLOW);
					break;
				case 3:
					distribution.push(ECommonsColor.GREEN);
					break;
				case 4:
					distribution.push(ECommonsColor.CYAN);
					break;
				case 5:
					distribution.push(ECommonsColor.BLUE);
					break;
				case 6:
					distribution.push(ECommonsColor.INDIGO);
					break;
				case 7:
					distribution.push(ECommonsColor.MAGENTA);
					break;
			}
			
			rainbowIndex += rainbowInc;
		}
		
		return distribution;
	}

	public static allocateColor(color: string|ECommonsColor|ECommonsPalette, sanitized: DomSanitizer): ICommonsColor {
		if (color === ECommonsColor.RED) return { classColor: fromECommonsColor(ECommonsColor.RED) };
		if (color === ECommonsColor.ORANGE) return { classColor: fromECommonsColor(ECommonsColor.ORANGE) };
		if (color === ECommonsColor.YELLOW) return { classColor: fromECommonsColor(ECommonsColor.YELLOW) };
		if (color === ECommonsColor.GREEN) return { classColor: fromECommonsColor(ECommonsColor.GREEN) };
		if (color === ECommonsColor.CYAN) return { classColor: fromECommonsColor(ECommonsColor.CYAN) };
		if (color === ECommonsColor.BLUE) return { classColor: fromECommonsColor(ECommonsColor.BLUE) };
		if (color === ECommonsColor.INDIGO) return { classColor: fromECommonsColor(ECommonsColor.INDIGO) };
		if (color === ECommonsColor.MAGENTA) return { classColor: fromECommonsColor(ECommonsColor.MAGENTA) };
		if (color === ECommonsPalette.PRIMARY.toString()) return { classColor: fromECommonsPalette(ECommonsPalette.PRIMARY) };
		if (color === ECommonsPalette.PRIMARY_LIGHT.toString()) return { classColor: fromECommonsPalette(ECommonsPalette.PRIMARY_LIGHT) };
		if (color === ECommonsPalette.PRIMARY_DARK.toString()) return { classColor: fromECommonsPalette(ECommonsPalette.PRIMARY_DARK) };
		if (color === ECommonsPalette.SECONDARY.toString()) return { classColor: fromECommonsPalette(ECommonsPalette.SECONDARY) };
		if (color === ECommonsPalette.SECONDARY_LIGHT.toString()) return { classColor: fromECommonsPalette(ECommonsPalette.SECONDARY_LIGHT) };
		if (color === ECommonsPalette.SECONDARY_DARK.toString()) return { classColor: fromECommonsPalette(ECommonsPalette.SECONDARY_DARK) };
		
		if (('string' === typeof color) && (
				/^#[0-9a-f]{3}$/i.test(color)
				|| /^#[0-9a-f]{6}$/i.test(color)
				|| /^rgb(a?)\([ 0-9.,]+\)$/i.test(color)
		)) return { styleColor: sanitized.bypassSecurityTrustStyle(`fill:${color}`) };
		
		return {};
	}
	
	public static prettyNumber(value: number): string {
		return new CommonsPrettyFigurePipe().transform(value);
	}
	
	public static prettyPercent(value: number, sigma: number): string {
		return (value / Math.max(1, sigma)).toLocaleString(undefined, { style: 'percent', maximumSignificantDigits: 3 });
	}

	public static getScaleWidth(values: number[]): number {
		if (values.length === 0) return 1;
		
		return Math.max(...values
				.map((value: number): number => {
					const render: string = GraphicsCharts.prettyNumber(value);
					const digits: number = render.replace(/[^0-9]/g, '').length;
					const symbols: number = render.replace(/[^-.,]/g, '').length;
					return (digits * 2) + symbols;
				})
		);
	}

	public static getScaleMax(max: number): number {
		if (max === 0) return 0;

		const digits: number = Math.floor(Math.log10(max)) + 1;
		const tenScale: number = Math.pow(10, digits);
		
		if (max === (tenScale / 10)) {
			// exactly 10, 100, 1000 etc.
			return max;
		}
		
		let baseScale: number = tenScale;
		if (max <= (tenScale / 2)) baseScale = tenScale / 2;
		if (max <= (tenScale / 5)) baseScale = tenScale / 5;
		
		return baseScale;
	}

	public static listIntervals(max: number, dividor: number = 5): number[] {
		const interval: number = GraphicsCharts.getScaleMax(max) / dividor;

		const intervals: number[] = [ 0 ];
		for (let i = 1; i <= dividor; i++) intervals.push(i * interval);
		
		return intervals;
	}
	
	public static movingAverage(xys: TCommonsXy[], period: number): TCommonsXy[] {
		if (period === 0) return xys;
		
		if (xys.length < (2 + (period * 2))) return [];
		
		const averaged: TCommonsXy[] = [];
		const prev: TCommonsXy[] = [];

		let index: number = 0;
		for (const xy of xys) {
			if (prev.length >= period) {
				const average: number =
						xy.y
						+ prev.reduce((acc: number, current: TCommonsXy): number => acc + current.y, 0)
						+ xys.slice(index, index + period).reduce((acc: number, current: TCommonsXy): number => acc + current.y, 0);
				
				averaged.push({ x: xy.x, y: average / (1 + (period * 2)) });
			}
			
			prev.push(xy);
			while (prev.length > period) prev.shift();
			
			index++;
			if (index + period >= xys.length) break;
		}
		
		return averaged;
	}
}
