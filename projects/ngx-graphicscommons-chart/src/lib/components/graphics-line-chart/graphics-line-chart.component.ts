import { Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SafeStyle } from '@angular/platform-browser';

import * as SVGCatmullRomSpline from 'svg-catmull-rom-spline';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { GraphicsCharts } from '../../helpers/graphics-charts';

import { ICommonsLine } from '../../interfaces/icommons-line';
import { ICommonsColor } from '../../interfaces/icommons-color';

import { TCommonsXy } from 'ngx-graphicscommons-core';

interface IItem extends ICommonsColor {
		xy: TCommonsXy;
		name: string;
		expanded: boolean;
}

@Component({
		selector: 'graphics-line-chart',
		templateUrl: './graphics-line-chart.component.html',
		styleUrls: ['./graphics-line-chart.component.less']
})
export class GraphicsLineChartComponent extends CommonsComponent {
	@Input() lines: ICommonsLine[] = [];
	@Input() average: number = 0;
	@Input() maxValue?: number;
	@Input() xIntervalThreshold: number = 13;
	@Input() color: string = 'primary';
	@Input() top: number = 0;
	@Input() tail: number = 0;

	constructor(
			private sanitized: DomSanitizer
	) {
		super();
	}
	
	listItems(): IItem[] {
		const interval: number = this.lines.length < (this.xIntervalThreshold + 1) ? 1 : Math.ceil(this.lines.length / this.xIntervalThreshold);
		
		// first allocate interval names
		const names: string[] = this.lines
				.map((line: ICommonsLine, i: number): string => (i % interval) === 0 ? line.name : '');

		// now create gaps to hold any majors
		let index: number = 0;
		for (const line of this.lines) {
			if (line.major) {
				for (let i = -interval; i <= interval; i++) {
					const index2: number = index + i;
					if (index2 >= 0 && index2 < names.length) names[index2] = '';
				}
			}
			index++;
		}
		
		// now finally plug the gaps with any majors
		index = 0;
		for (const line of this.lines) {
			if (line.major) names[index] = line.name;
			index++;
		}

		return this.lines
				.map((line: ICommonsLine, i: number): IItem => {
					return Object.assign({}, GraphicsCharts.allocateColor(this.color, this.sanitized), {
						xy: {
								x: ((i + 0.5) / this.lines.length) * 100,
								y: 100 * (1 - this.getValuePosition(line.value))
						},
						name: names[i],
						expanded: (line.major ? true : false) || interval > 1
					});
				});
	}

	getStyleColor(): SafeStyle|undefined {
		const c: ICommonsColor = GraphicsCharts.allocateColor(this.color, this.sanitized);
		return c.styleColor;
	}

	getClassColor(): string|undefined {
		const c: ICommonsColor = GraphicsCharts.allocateColor(this.color, this.sanitized);
		return c.classColor;
	}
	
	getPath(): string {
		const items: IItem[] = this.listItems();
		
		for (let i = this.top; i-- > 0;) items.shift();
		for (let i = this.tail; i-- > 0;) items.pop();
		
		if (items.length < (2 + (this.average * 2))) return '';
		
		const points: number[][] = GraphicsCharts
				.movingAverage(
						items.map((i: IItem): TCommonsXy => i.xy),
						this.average
				)
				.map((xy: TCommonsXy): number[] => [ xy.x, xy.y ]);
		
		const tolerance = 4;
		const highestQuality = true;
		const attribute = SVGCatmullRomSpline.toPath(
				points,
				tolerance,
				highestQuality
		);
		
		return attribute;
	}

	getScaleValue(value: number): string {
		return value.toLocaleString(undefined, { style: 'decimal' });
	}
	
	getScaleWidth(): number {
		return GraphicsCharts.getScaleWidth(this.lines
				.map((line: ICommonsLine): number => Math.round(line.value)));
	}
	
	getMaxValue(): number {
		if (this.maxValue) return this.maxValue;
		
		if (this.lines.length === 0) return 0;
		
		return Math.max(
				...this.lines
						.map((line: ICommonsLine): number => line.value)
		);
	}
	
	listIntervals(): number[] {
		return GraphicsCharts.listIntervals(GraphicsCharts.getScaleMax(this.getMaxValue()));
	}
	
	getValuePosition(value: number): number {
		if (this.maxValue) {
			if (this.maxValue === 0) return 0;
			return value / this.maxValue;
		}

		const baseScale: number = GraphicsCharts.getScaleMax(this.getMaxValue());
		if (baseScale === 0) return 0;
		
		return value / baseScale;
	}
}
