declare module 'svg-catmull-rom-spline' {
	namespace SVGCatmullRomSpline {
	}
	
	const _default: {
			toPoints: (points: number[][], tolerance: number, highestQuality: boolean) => number[][],
			toPath: (points: number[][], tolerance: number, highestQuality: boolean) => string
	};
	
	export = _default;
}
