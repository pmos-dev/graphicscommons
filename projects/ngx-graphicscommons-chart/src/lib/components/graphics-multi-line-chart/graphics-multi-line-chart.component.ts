import { Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SafeStyle } from '@angular/platform-browser';

import * as SVGCatmullRomSpline from 'svg-catmull-rom-spline';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { GraphicsCharts } from '../../helpers/graphics-charts';

import { ICommonsMultiLine } from '../../interfaces/icommons-multi-line';
import { ICommonsColor } from '../../interfaces/icommons-color';

import { TCommonsXy } from 'ngx-graphicscommons-core';

interface IItem extends ICommonsColor {
		xy: TCommonsXy;
		name: string;
		expanded: boolean;
}

@Component({
		selector: 'graphics-multi-line-chart',
		templateUrl: './graphics-multi-line-chart.component.html',
		styleUrls: ['./graphics-multi-line-chart.component.less']
})
export class GraphicsMultiLineChartComponent extends CommonsComponent {
	@Input() lines: ICommonsMultiLine[] = [];
	@Input() average: number = 0;
	@Input() maxValue?: number;
	@Input() xIntervalThreshold: number = 13;
	@Input() colors: string[] = [];

	constructor(
			private sanitized: DomSanitizer
	) {
		super();
	}
	
	listListSetIndexes(): number[] {
		const max: number = Math.max(...this.lines
				.map((line: ICommonsMultiLine): number => line.values.length));
		
		const nums: number[] = [];
		for (let i = 0; i < max; i++) nums.push(i);
		
		return nums;
	}
	
	listItems(listSet: number): IItem[] {
		const interval: number = this.lines.length < (this.xIntervalThreshold + 1) ? 1 : Math.ceil(this.lines.length / this.xIntervalThreshold);
		
		// first allocate interval names
		const names: string[] = this.lines
				.map((line: ICommonsMultiLine, i: number): string => (i % interval) === 0 ? line.name : '');

		// now create gaps to hold any majors
		let index: number = 0;
		for (const line of this.lines) {
			if (line.major) {
				for (let i = -interval; i <= interval; i++) {
					const index2: number = index + i;
					if (index2 >= 0 && index2 < names.length) names[index2] = '';
				}
			}
			index++;
		}
		
		// now finally plug the gaps with any majors
		index = 0;
		for (const line of this.lines) {
			if (line.major) names[index] = line.name;
			index++;
		}

		return this.lines
				.map((line: ICommonsMultiLine, i: number): IItem => {
					return Object.assign({}, GraphicsCharts.allocateColor(this.colors[listSet], this.sanitized), {
						xy: {
								x: ((i + 0.5) / this.lines.length) * 100,
								y: 100 * (1 - this.getValuePosition(line.values[listSet]))
						},
						name: names[i],
						expanded: (line.major ? true : false) || interval > 1
					});
				});
	}

	getStyleColor(listSet: number): SafeStyle|undefined {
		const c: ICommonsColor = GraphicsCharts.allocateColor(this.colors[listSet], this.sanitized);
		return c.styleColor;
	}

	getClassColor(listSet: number): string|undefined {
		const c: ICommonsColor = GraphicsCharts.allocateColor(this.colors[listSet], this.sanitized);
		return c.classColor;
	}
	
	getPath(listSet: number): string {
		const items: IItem[] = this.listItems(listSet);
		
		if (items.length < (2 + (this.average * 2))) return '';
		
		const points: number[][] = GraphicsCharts
				.movingAverage(
						items.map((i: IItem): TCommonsXy => i.xy),
						this.average
				)
				.map((xy: TCommonsXy): number[] => [ xy.x, xy.y ]);
		
		const tolerance = 4;
		const highestQuality = true;
		const attribute = SVGCatmullRomSpline.toPath(
				points,
				tolerance,
				highestQuality
		);
		
		return attribute;
	}

	getScaleValue(value: number): string {
		return value.toLocaleString(undefined, { style: 'decimal' });
	}
	
	getScaleWidth(): number {
		return GraphicsCharts.getScaleWidth(this.lines
				.map((line: ICommonsMultiLine): number => Math.round(Math.max(...line.values))));
	}
	
	getMaxValue(): number {
		if (this.maxValue) return this.maxValue;
		
		if (this.lines.length === 0) return 0;
		
		return Math.max(
				...this.lines
						.map((line: ICommonsMultiLine): number => Math.max(...line.values))
		);
	}
	
	listIntervals(): number[] {
		return GraphicsCharts.listIntervals(GraphicsCharts.getScaleMax(this.getMaxValue()));
	}
	
	getValuePosition(value: number): number {
		if (this.maxValue) {
			if (this.maxValue === 0) return 0;
			return value / this.maxValue;
		}

		const baseScale: number = GraphicsCharts.getScaleMax(this.getMaxValue());
		if (baseScale === 0) return 0;
		
		return value / baseScale;
	}
}
