import { Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SafeStyle } from '@angular/platform-browser';

import { ECommonsColor } from 'tscommons-graphics';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { GraphicsCharts } from '../../helpers/graphics-charts';

import { ICommonsSegment } from '../../interfaces/icommons-segment';
import { ICommonsColor } from '../../interfaces/icommons-color';

import { TCommonsXy } from 'ngx-graphicscommons-core';

import { ECommonsLabelType } from '../../enums/ecommons-label-type';

interface IItem extends ICommonsColor {
	from: number;
	to: number;
	label?: string;
}

const RADIANS: number = Math.PI * 2;
const SIZE: number = 100;

@Component({
	selector: 'graphics-pie-chart',
	templateUrl: './graphics-pie-chart.component.html',
	styleUrls: ['./graphics-pie-chart.component.less']
})
export class GraphicsPieChartComponent extends CommonsComponent {
	private static trig(percent: number): TCommonsXy {
		const o = RADIANS * percent;
		const ro = (RADIANS / 4) - o;
		
		return {
			x: Math.cos(ro),
			y: 1 - Math.sin(ro)
		};
	}

	SIZE: number = SIZE;
	
	@Input() segments: ICommonsSegment[] = [];
	@Input() labelType: ECommonsLabelType|undefined;
	@Input() indent: number = 0.8;
	@Input() minPercentLabel: number = 0;

	constructor(
			private sanitized: DomSanitizer
	) {
		super();
	}
	
	listItems(): IItem[] {
		const sigma: number = this.segments
				.map((segment: ICommonsSegment): number => Math.abs(segment.value))
				.reduce((acc: number, value: number): number => acc + value, 0);
			
		if (sigma === 0) return [];

		const unsetColor: number = this.segments
				.filter((segment: ICommonsSegment): boolean => segment.color === undefined ? true : false)
				.length;
		const rainbowProvision: ECommonsColor[] = GraphicsCharts.listRainbowDistribution(unsetColor);

		let rainbowIndex: number = 0;
		let previous: number = 0;
		return this.segments
				.map((segment: ICommonsSegment): IItem => {
					const perc: number = Math.abs(segment.value) / sigma;
					const f: number = previous;
					const t: number = previous + perc;
					previous += perc;
						
					const color: string|ECommonsColor = segment.color !== undefined ? segment.color : rainbowProvision[rainbowIndex++];

					let label: string|undefined = segment.label;
					if (label === undefined) {
						if (this.labelType === ECommonsLabelType.NUMBER) label = GraphicsCharts.prettyNumber(segment.value);
						if (this.labelType === ECommonsLabelType.PERCENT) label = GraphicsCharts.prettyPercent(segment.value, sigma);
					}
					
					if (perc < this.minPercentLabel) label = undefined;
						
					return Object.assign({}, GraphicsCharts.allocateColor(color, this.sanitized), {
							from: f,
							to: t,
							label: label
					});
				});
	}
	
	getTextPosition(item: IItem): SafeStyle {
		const middle: number = (item.from + item.to) / 2;
		
		const t: TCommonsXy = GraphicsPieChartComponent.trig(middle);
		
		const s: TCommonsXy = {
				x: ((100 * this.indent) / 2) * (1 + t.x),
				y: ((100 * this.indent) / 2) * (t.y)
		};
		s.x += (100 * (1 - this.indent)) / 2;
		s.y += (100 * (1 - this.indent)) / 2;
		
		return this.sanitized.bypassSecurityTrustStyle(`top:${s.y}%;left:${s.x}%`);
	}

	getD(item: IItem): string {
		const start: number = Math.min(item.from, item.to);
		const end: number = Math.max(item.from, item.to);
		
		const st: TCommonsXy = GraphicsPieChartComponent.trig(start);
		const et: TCommonsXy = GraphicsPieChartComponent.trig(end);
		
		const radius: number = SIZE / 2;
		
		const s: TCommonsXy = {
				x: radius * (1 + st.x),
				y: radius * st.y
		};
		
		const e: TCommonsXy = {
				x: radius * (1 + et.x),
				y: radius * et.y
		};

		const period: number = end - start;
		
		const long: number = period > 0.5 ? 1 : 0;
		
		return `m ${s.x},${s.y} A ${radius},${radius} 0 ${long} 1 ${e.x},${e.y} L ${radius},${radius} Z`;
	}
}
