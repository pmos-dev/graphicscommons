import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { ECommonsColor } from 'tscommons-graphics';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { GraphicsCharts } from '../../helpers/graphics-charts';

import { ICommonsBar } from '../../interfaces/icommons-bar';
import { ICommonsColor } from '../../interfaces/icommons-color';

import { ECommonsLabelType } from '../../enums/ecommons-label-type';

interface IItem extends ICommonsColor {
		height: number;
		name: string;
		expanded: boolean;
		label?: string;
}

@Component({
		selector: 'graphics-bar-chart',
		templateUrl: './graphics-bar-chart.component.html',
		styleUrls: ['./graphics-bar-chart.component.less']
})
export class GraphicsBarChartComponent extends CommonsComponent implements OnInit, OnChanges {
	@Input() bars: ICommonsBar[] = [];
	@Input() labelType: ECommonsLabelType|undefined;
	@Input() private maxValue?: number;
	@Input() private minValue?: number;
	@Input() xIntervalThreshold: number = 13;
	
	internalItems: IItem[] = [];
	internalScaleWidth: number = 0;
	internalMaxValue: number = 0;
	internalListIntervals: number[] = [];
	internalIntervalPositions: number[] = [];

	constructor(
			private sanitized: DomSanitizer
	) {
		super();
	}
	
	ngOnInit(): void {
		super.ngOnInit();
		
		this.refresh();
	}

	ngOnChanges(_changes: SimpleChanges): void {
		this.refresh();
	}
	
	private refresh() {
		this.internalScaleWidth = this.getScaleWidth();
		this.internalMaxValue = this.getMaxValue();
		this.internalItems = this.listItems();
		this.internalListIntervals = this.listIntervals();
		
		this.internalIntervalPositions = this.internalListIntervals
				.map((step: number): number => 100 * (1 - this.getValuePosition(step)));
	}

	private listItems(): IItem[] {
		const interval: number = this.bars.length < (this.xIntervalThreshold + 1) ? 1 : Math.ceil(this.bars.length / this.xIntervalThreshold);
		
		// first allocate interval names
		const names: string[] = this.bars
				.map((bar: ICommonsBar, i: number): string => (i % interval) === 0 ? bar.name : '');

		// now create gaps to hold any majors
		let index: number = 0;
		for (const bar of this.bars) {
			if (bar.major !== false) {
				for (let i = -interval; i <= interval; i++) {
					const index2: number = index + i;
					if (index2 >= 0 && index2 < names.length) names[index2] = '';
				}
			}
			index++;
		}
		
		// now finally plug the gaps with any majors
		index = 0;
		for (const bar of this.bars) {
			if (bar.major !== false) names[index] = bar.name;
			index++;
		}

		const unsetColor: number = this.bars
				.filter((bar: ICommonsBar): boolean => bar.color === undefined ? true : false)
				.length;
		const rainbowProvision: ECommonsColor[] = GraphicsCharts.listRainbowDistribution(unsetColor);

		let rainbowIndex: number = 0;
		return this.bars
				.map((bar: ICommonsBar, i: number): IItem => {
					const color: string|ECommonsColor = bar.color !== undefined ? bar.color : rainbowProvision[rainbowIndex++];

					let label: string|undefined = bar.label;
					if (label === undefined) {
						if (this.labelType === ECommonsLabelType.NUMBER && bar.value > (this.internalMaxValue * 0.2)) label = GraphicsCharts.prettyNumber(bar.value);
					}
						
					return Object.assign({}, GraphicsCharts.allocateColor(color, this.sanitized), {
						height: this.getValuePosition(bar.value),
						name: names[i],
						label: label,
						expanded: (bar.major ? true : false) || interval > 1
					});
				});
	}

	private getScaleWidth() {
		return 7;	// commonsPrettyFigure is never more than 5 +2 for space
//		return GraphicsCharts.getScaleWidth(this.bars
//				.map((bar: ICommonsBar): number => Math.round(bar.value)));
	}
	
	private getMaxValue(): number {
		if (this.maxValue) return this.maxValue;
		
		if (this.bars.length === 0) return 0;
		
		return Math.max(...[
				(this.minValue ? this.minValue : 1),
				...this.bars
						.map((bar: ICommonsBar): number => bar.value)
		]);
	}
	
	private listIntervals(): number[] {
		return GraphicsCharts.listIntervals(GraphicsCharts.getScaleMax(this.internalMaxValue));
	}
	
	getValuePosition(value: number): number {
		if (this.maxValue) {
			if (this.maxValue === 0) return 0;
			return value / this.internalMaxValue;
		}

		const baseScale: number = GraphicsCharts.getScaleMax(this.internalMaxValue);
		if (baseScale === 0) return 0;
		
		return value / baseScale;
	}
}
