import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { ECommonsColor } from 'tscommons-graphics';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { GraphicsCharts } from '../../helpers/graphics-charts';

import { ICommonsRange } from '../../interfaces/icommons-range';
import { ICommonsColor } from '../../interfaces/icommons-color';

import { ECommonsLabelType } from '../../enums/ecommons-label-type';

interface IItem extends ICommonsColor {
	upper: number;
	height: number;
	name: string;
	expanded: boolean;
	label?: string;
}

@Component({
	selector: 'graphics-range-bar-chart',
	templateUrl: './graphics-range-bar-chart.component.html',
	styleUrls: ['./graphics-range-bar-chart.component.less']
})
export class GraphicsRangeBarChartComponent extends CommonsComponent implements OnInit, OnChanges {
	@Input() ranges: ICommonsRange[] = [];
	@Input() labelType: ECommonsLabelType|undefined;
	@Input() private maxValue?: number;
	@Input() private minValue?: number;
	@Input() xIntervalThreshold: number = 13;
	
	internalItems: IItem[] = [];
	internalScaleWidth: number = 0;
	internalMaxValue: number = 0;
	internalListIntervals: number[] = [];
	internalIntervalPositions: number[] = [];

	constructor(
			private sanitized: DomSanitizer
	) {
		super();
	}
	
	ngOnInit(): void {
		super.ngOnInit();
		
		this.refresh();
	}

	ngOnChanges(_changes: SimpleChanges): void {
		this.refresh();
	}
	
	private refresh() {
		this.internalScaleWidth = this.getScaleWidth();
		this.internalMaxValue = this.getMaxValue();
		this.internalItems = this.listItems();
		this.internalListIntervals = this.listIntervals();
		
		this.internalIntervalPositions = this.internalListIntervals
				.map((step: number): number => 100 * (1 - this.getUpper(step)));
	}

	private listItems(): IItem[] {
		const interval: number = this.ranges.length < (this.xIntervalThreshold + 1) ? 1 : Math.ceil(this.ranges.length / this.xIntervalThreshold);
		
		// first allocate interval names
		const names: string[] = this.ranges
				.map((bar: ICommonsRange, i: number): string => (i % interval) === 0 ? bar.name : '');

		// now create gaps to hold any majors
		let index: number = 0;
		for (const range of this.ranges) {
			if (range.major !== false) {
				for (let i = -interval; i <= interval; i++) {
					const index2: number = index + i;
					if (index2 >= 0 && index2 < names.length) names[index2] = '';
				}
			}
			index++;
		}
		
		// now finally plug the gaps with any majors
		index = 0;
		for (const range of this.ranges) {
			if (range.major !== false) names[index] = range.name;
			index++;
		}

		const unsetColor: number = this.ranges
				.filter((range: ICommonsRange): boolean => range.color === undefined ? true : false)
				.length;
		const rainbowProvision: ECommonsColor[] = GraphicsCharts.listRainbowDistribution(unsetColor);

		let rainbowIndex: number = 0;
		return this.ranges
				.map((range: ICommonsRange, i: number): IItem => {
					const color: string|ECommonsColor = range.color !== undefined ? range.color : rainbowProvision[rainbowIndex++];

//					let label: string|undefined = range.label;
//					if (label === undefined) {
//						if (this.labelType === ECommonsLabelType.NUMBER && range.value > (this.internalMaxValue * 0.2)) label = GraphicsCharts.prettyNumber(range.value);
//					}
						
					return Object.assign({}, GraphicsCharts.allocateColor(color, this.sanitized), {
						upper: this.getUpper(range.to),
						height: this.getHeight(range.from, range.to),
						name: names[i],
						label: range.label,
						expanded: (range.major ? true : false) || interval > 1
					});
				});
	}

	private getScaleWidth() {
		return 7;	// commonsPrettyFigure is never more than 5 +2 for space
//		return GraphicsCharts.getScaleWidth(this.ranges
//				.map((range: ICommonsRange): number => Math.round(range.value)));
	}
	
	private getMaxValue(): number {
		if (this.maxValue) return this.maxValue;
		
		if (this.ranges.length === 0) return 0;
		
		return Math.max(...[
				(this.minValue ? this.minValue : 1),
				...this.ranges
						.map((range: ICommonsRange): number => range.from),
				...this.ranges
						.map((range: ICommonsRange): number => range.to)
		]);
	}
	
	private listIntervals(): number[] {
		return GraphicsCharts.listIntervals(GraphicsCharts.getScaleMax(this.internalMaxValue));
	}
	
	getUpper(to: number): number {
		if (this.maxValue) {
			if (this.maxValue === 0) return 0;
			return to / this.internalMaxValue;
		}

		const baseScale: number = GraphicsCharts.getScaleMax(this.internalMaxValue);
		if (baseScale === 0) return 0;
		
		return to / baseScale;
	}
	
	getHeight(from: number, to: number): number {
		if (this.maxValue) {
			if (this.maxValue === 0) return 0;
			return (to - from) / this.internalMaxValue;
		}

		const baseScale: number = GraphicsCharts.getScaleMax(this.internalMaxValue);
		if (baseScale === 0) return 0;
		
		return (to - from) / baseScale;
	}
}
