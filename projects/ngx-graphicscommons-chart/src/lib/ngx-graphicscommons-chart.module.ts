import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsPipeModule } from 'ngx-angularcommons-pipe';

import { NgxGraphicsCommonsCoreModule } from 'ngx-graphicscommons-core';

import { GraphicsPieChartComponent } from './components/graphics-pie-chart/graphics-pie-chart.component';
import { GraphicsBarChartComponent } from './components/graphics-bar-chart/graphics-bar-chart.component';
import { GraphicsLineChartComponent } from './components/graphics-line-chart/graphics-line-chart.component';
import { GraphicsMultiLineChartComponent } from './components/graphics-multi-line-chart/graphics-multi-line-chart.component';
import { GraphicsRangeBarChartComponent } from './components/graphics-range-bar-chart/graphics-range-bar-chart.component';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsPipeModule,
				NgxGraphicsCommonsCoreModule
		],
		declarations: [
				GraphicsPieChartComponent,
				GraphicsBarChartComponent,
				GraphicsLineChartComponent,
				GraphicsMultiLineChartComponent,
				GraphicsRangeBarChartComponent
		],
		exports: [
				GraphicsPieChartComponent,
				GraphicsBarChartComponent,
				GraphicsLineChartComponent,
				GraphicsMultiLineChartComponent,
				GraphicsRangeBarChartComponent
		]
})
export class NgxGraphicsCommonsChartModule { }
