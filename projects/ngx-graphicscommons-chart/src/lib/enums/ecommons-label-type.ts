import { CommonsType } from 'tscommons-core';

export enum ECommonsLabelType {
		NONE = 'none',
		NUMBER = 'number',
		PERCENT = 'percent'
}

export function toECommonsLabelType(type: string): ECommonsLabelType|undefined {
	switch (type) {
		case ECommonsLabelType.NONE.toString():
			return ECommonsLabelType.NONE;
		case ECommonsLabelType.NUMBER.toString():
			return ECommonsLabelType.NUMBER;
		case ECommonsLabelType.PERCENT.toString():
			return ECommonsLabelType.PERCENT;
	}
	return undefined;
}

export function fromECommonsLabelType(type: ECommonsLabelType): string {
	switch (type) {
		case ECommonsLabelType.NONE:
			return ECommonsLabelType.NONE.toString();
		case ECommonsLabelType.NUMBER:
			return ECommonsLabelType.NUMBER.toString();
		case ECommonsLabelType.PERCENT:
			return ECommonsLabelType.PERCENT.toString();
	}
	
	throw new Error('Unknown ECommonsLabelType');
}

export function isECommonsLabelType(test: unknown): test is ECommonsLabelType {
	if (!CommonsType.isString(test)) return false;
	
	return toECommonsLabelType(test) !== undefined;
}

export function keyToECommonsLabelType(key: string): ECommonsLabelType {
	switch (key) {
		case 'NONE':
			return ECommonsLabelType.NONE;
		case 'NUMBER':
			return ECommonsLabelType.NUMBER;
		case 'PERCENT':
			return ECommonsLabelType.PERCENT;
	}
	
	throw new Error(`Unable to obtain ECommonsLabelType for key: ${key}`);
}

export const ECOMMONS_LABEL_TYPES: ECommonsLabelType[] = Object.keys(ECommonsLabelType)
		.map((e: string): ECommonsLabelType => keyToECommonsLabelType(e));
